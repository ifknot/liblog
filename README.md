# README #
![free as in beer](https://cldup.com/raLE16TIIY.png)
Logging is a critical technique for troubleshooting and maintaining software systems. Presented is a _Beta_ release of a C\++11 lightweight logging framework that is simple to use, thread-safe, and portable. It uses some nifty ideas like _active objects_ and some neat C\++11 programming techniques such as _variadic member functions_.

### Why liblog? ###
Multithreading is why. Because, despite writing log messages to a file being readily acheived with some roll-your-own `#define` functions, when you write log messages from different threads without some form of synchronization you get this:
```sh
0000211,09/Sep/2016:21:30000212,09/Sep/2016:21:31:38 +0100,thread B log message
1:38 +0100,thread A log message
```
With `liblog` you get this:
```sh
0000211,09/Sep/2016:21:31:38 +0100,thread A log message
0000212,09/Sep/2016:21:31:38 +0100,thread B log message
```
Yes there are others but this is mine, it's simple and easy to use.
### Licence ###
`liblog`is free (as in beer) software and is licensed under the [GNU Lesser General Public License (LGPL) version 3](https://www.gnu.org/licenses/lgpl-3.0.en.html).

### Usage ###

```cpp
log::active_log DEBUG("test.log");
DEBUG("hello ", "world! ", 100);
```

### What is this repository for? ###

* ಠ_ರೃ `liblog` Lightweight, multi-purpose, multi-streamed, multi-threaded, synchronised logging using active-objects for all your record writing needs!
* Version(β) 0.1
* Inspiration: [A Lightweight Logger for C++ by Filip Janiszewski, January 31, 2013](http://www.drdobbs.com/cpp/a-lightweight-logger-for-c/240147505) & [Prefer Using Active Objects Instead of Naked Threads by Herb Sutter, June 14, 2010](http://www.drdobbs.com/parallel/prefer-using-active-objects-instead-of-n/225700095?pgno=1)

### How do I get set up? ###

* `liblog` Everything that you need (4 header files and a source file) is contained within the **liblog** folder.
* __Configuration__ The `active_log_ini.h` header file contains options for the message queue capacity, separator type & log file first line message.
```cpp
namespace log {
    constexpr size_t CAPACITY = 1024; //
    constexpr char CSV = ','; // use '\t' for tab seperated values
    const std::string INFO_MESSAGE = "Active Object Logger (β) v0.1 Fields: line_number, CLF_time, message";
}
```
* __Time Format__ The logger defaults to using the Common Log Format(CLF) time format [10/Oct/2000:13:55:36 -0700] as the date, time, and time zone that the message was logged (default format string "%d/%b/%Y:%H:%M:%S %z"). This can be modified by instantiating the log class with an additional [time format string](http://en.cppreference.com/w/cpp/io/manip/put_time) e.g.
```cpp
log::active_log TRACE("test.log", "%c %Z"); // Fri Sep  9 22:02:00 2016 BST
```
* __Dependencies__ The logging software is written entirely in native C++11 and has no dependencies other than a compiler that supports the required C++11 features. However, the included unit test file is dependent upon the [Bandit](http://banditcpp.org/) human firendly unit testing framework and the included project file is for the excellent cross-platform [Qt Creator IDE](https://www.qt.io/ide/).
* __How to run tests__ The unit tests a written for the [Bandit](http://banditcpp.org/) human firendly unit testing framework consisting of 15 tests in one test source file, 13 for the queue used by the active output stream, 1 for the active output stream that underlies the active log and 1 for the active log itself. The latter should generate a test log file "test.log"
```sh
0000000,09/Sep/2016:21:31:38 +0100,Active Object Logger(β) v0.1 Fields: line_number, CLF_time, message
0000001,09/Sep/2016:21:31:38 +0100,hello world! 100
```

### Contribution guidelines ###

* __Writing tests__ All tests are welcome _but_ must be submitted in the [Bandit](http://banditcpp.org/) unit testing framework format.
* __Code review__ All _constructive_ code review is welcome

### Who do I talk to? ###

* Repo owner: https://bitbucket.org/ifknot/
* Support: [@ifknot](https://twitter.com/ifknot)


