#include "active_log.h"

namespace liblog {

    active_log::active_log(const std::string filename, std::string time_format):
        aob::active_ostream<liblog::CAPACITY>(ofs),
        ofs(filename),
        start_time_(clock_t::now()),
        time_format_(time_format) {
        ofs.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        log_stream_ << prefix();
        this->operator ()(liblog::INFO_MESSAGE);
    }

    bool active_log::operator ()() {
        lock_guard_t mlock(mutex_);
        log_stream_ << suffix();
        send(log_stream_.str());
        log_stream_.str("");
        log_stream_ << prefix();
        return true;
    }

    std::string active_log::prefix() {
        std::stringstream prefix_;
        prefix_ << linenumber() << timestamp() << CSV;
        return prefix_.str();
    }

    std::string active_log::suffix() {
        return "\n";
    }

    std::string active_log::linenumber() {
        std::stringstream prefix_;
        prefix_.fill('0');
        prefix_.width(7);
        prefix_ << line_num_++ << CSV;
        return prefix_.str();
    }

    std::string active_log::timestamp() {
        std::stringstream timestamp_;
        std::time_t calendar_time = std::time(nullptr);
        timestamp_ << std::put_time(std::localtime(&calendar_time), time_format_.c_str());
        return timestamp_.str();
    }

    std::string active_log::uptime()  {
        std::stringstream uptime_;
        uptime_ << std::chrono::duration_cast<duration_t>(clock_t::now() - start_time_).count();
        return uptime_.str();
    }

}
