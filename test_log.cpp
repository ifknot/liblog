#include <thread>
#include <fstream>

#include <bandit/bandit.h>
#include <libclk/jittery_delay.h>

#include "mpmc_bounded_queue.h"
#include "active_log.h"

using namespace bandit;

using queue_t = que::mpmc_bounded_queue<int,1024>;

void pusher(queue_t& q, size_t a, size_t b) {
    for(size_t i = a; i < b; ++i) q.wait_push(i);
}

void popper(queue_t& q, unsigned int a, unsigned int b, std::atomic<int>& sum) {
    for(size_t i = a; i < b; ++i) sum += q.wait_pop();
}

go_bandit([](){

    describe("test log", [](){

        describe("test queue", [](){

            queue_t Q;
            int N{1024};

            it("should be empty", [&](){
                AssertThat(Q.empty(), Equals(true));
            });

            it("should have default size 0", [&](){
                AssertThat(Q.size_guess(), Equals(0));
            });

            it("should be false try_pop()", [&](){
                int i;
                AssertThat(Q.try_pop(i), Equals(false));
            });

            it("should push and wait_pop 1 leaving an empty queue", [&](){
                Q.wait_push(1);
                int i = Q.wait_pop();
                AssertThat(i, Equals(1));
                AssertThat(Q.empty(), Equals(true));
            });

            it("should push N ints", [&](){
                for(int i = 0; i < N; ++i) Q.wait_push(i);
                AssertThat(Q.size_guess(), Equals(N));
            });

            it("should be non-empty", [&](){
                AssertThat(Q.empty(), Equals(false));
            });

            it("should try_pop drain", [&](){
                int i;
                int n = 0;
                while (Q.try_pop(i)) {
                    AssertThat(i, Equals(n++));
                }
                AssertThat(N, Equals(n));
            });

            it("should be empty", [&](){
                AssertThat(Q.empty(), Equals(true));
            });

            it("should wait_push N ints", [&](){
                for(int i = 0; i < N; ++i) Q.wait_push(i);
                AssertThat(Q.size_guess(), Equals(N));
            });

            it("should be non-empty", [&](){
                AssertThat(Q.empty(), Equals(false));
            });

            it("should wait_pop to empty in order enqueued", [&](){
                for(int i = 0; i < N; ++i) {
                    AssertThat(Q.wait_pop(), Equals(i));
                }
            });

            it("should be empty", [&](){
                AssertThat(Q.empty(), Equals(true));
            });

            it("should n(n+1)/2 multithread wait_pop thrash", [&](){
                std::vector<std::thread> threads;
                size_t N = 1023;
                size_t q_size = N + 1;
                size_t pushers = 4;
                size_t poppers = 4;
                std::atomic<int> sum{0};
                for(size_t i = 0; i < poppers; ++i) {
                    threads.emplace_back(std::thread(popper, std::ref(Q), i * (q_size / poppers), (i + 1) * (q_size / poppers), std::ref(sum)));
                }
                for(size_t i = 0; i < pushers; ++i) {
                    threads.emplace_back(std::thread(pusher, std::ref(Q), i * (q_size / pushers), (i + 1) * (q_size / pushers)));
                }

                for(auto& t : threads) t.join();
                AssertThat(sum.load(), Equals((N * (N + 1) / 2)));
            });

        });


        describe("test active stream", [](){

            std::ofstream fout("test.txt");
            aob::active_ostream<1024> aout(fout);
            std::string test_line = "test line number =";

            it("should write 'test line =100' to file 'test.txt' ", [&](){
                aout << test_line << 100;
                while(aout.busy()) {}
                fout.close();
                std::ifstream fin;
                fin.exceptions(std::ifstream::failbit | std::ifstream::badbit);
                fin.open("test.txt");
                std::string line;
                std::getline(fin, line);
                AssertThat(line, Equals(test_line + "100"));
            });

        });

        describe("test active log", [](){

            liblog::active_log TRACE("test.log", "%c %Z");
            std::vector<std::unique_ptr<std::thread>> tt;

            it("should write a log line to 'test log'", [&](){
                AssertThat(TRACE("hello ", "world! ",100), Equals(true));
            });

            it("should spawn threads trying to log info", [&](){
                for(int i = 0; i < 10; ++i) {
                    tt.emplace_back(new std::thread([&]{
                        libclk::jittery_delay<std::chrono::microseconds> jjj(i * 10000);
                        for(int j = 0; j < 100; ++j) {
                            TRACE(std::this_thread::get_id(), "=", j);
                            jjj.busy_wait();
                        }
                    }));
                }
                for(auto& t : tt) t->join();
                AssertThat(TRACE.busy(), Equals(false));
            });



        });

        /*
        describe("", [](){

            it("", [&](){

                //AssertThat(, Equals());
            });

        });
        */

    });

});
