#ifndef ACTIVE_LOG_H
#define ACTIVE_LOG_H

#include <iostream>
#include <sstream>
#include <fstream>
#include <ctime>
#include <iomanip>
#include <chrono>
#include <mutex>

#include "active_ostream.h"
#include "active_log_ini.h"

namespace liblog {

    class active_log: public aob::active_ostream<liblog::CAPACITY> {

        using clock_t = std::chrono::high_resolution_clock;
        using duration_t = std::chrono::microseconds;
        using mutex_t = std::recursive_mutex;
        using lock_guard_t = std::lock_guard<mutex_t>;

    public:

        active_log(const std::string filename, std::string time_format = "%d/%b/%Y:%H:%M:%S %z");// Common Log Format (CLF) time format

        virtual std::string prefix();

        virtual std::string suffix();

        std::string linenumber();

        std::string timestamp();

        std::string uptime();

        bool operator()();

        template<typename Arg, typename...Args>
        bool operator ()(Arg arg, Args... args) {
            lock_guard_t mlock(mutex_);
            log_stream_ << arg;
            return this->operator()(args...);
        }

        virtual ~active_log() = default;

    private:

        template<typename T>
        active_ostream& operator<<(const T&) {}

        void send(message_t m) override final {
            aob::active_ostream<liblog::CAPACITY>::send(m);
        }

        std::ofstream ofs;
        std::chrono::time_point<clock_t> start_time_;
        std::string time_format_;
        std::stringstream log_stream_;
        unsigned line_num_ = 0;
        mutex_t mutex_;

    };

}

#endif // ACTIVE_LOG_H
