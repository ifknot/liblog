#ifndef JITTERY_DELAY_H
#define JITTERY_DELAY_H

#include <chrono>
#include <random>

namespace libclk {

    template<typename TDuration = std::chrono::microseconds, typename TClock = std::chrono::high_resolution_clock>
    struct jittery_delay {

        jittery_delay(unsigned int delay, float jitter = 0.1):
            delay(delay),
            jiffie(0),
            jitter(jitter) {}

        TDuration busy_wait(bool yield = false) {
            std::random_device rnd;
            std::mt19937 gen(rnd());
            std::uniform_real_distribution<> d(-jitter, jitter);
            jiffie = delay * d(gen);
            TDuration wait(delay + jiffie);

            then = std::chrono::high_resolution_clock::now();
            when = then + wait;

            while (then < when) {
                then = std::chrono::high_resolution_clock::now();
                if (yield) {
                    std::this_thread::yield();
                }
            }

            return std::chrono::duration_cast<TDuration>(then - when);
        }

        unsigned int delay;
        int jiffie;
        float jitter;
        std::chrono::time_point<TClock> then, when;

    };

}

#endif // JITTERY_DELAY_H
