#ifndef STOPWATCH_H
#define STOPWATCH_H
#include <math.h>
#include <algorithm>
#include <sstream>

namespace clk {

    template<typename TDuration = std::chrono::microseconds, typename TClock = std::chrono::high_resolution_clock>
    class stopwatch {

    public:

        stopwatch(): before(TClock::now()), after(before) {}

        void restart() {
            before = after = TClock::now();
        }

        double stop() {
            after = TClock::now();
            return elapsed();
        }

        double elapsed() {
            auto duration = std::chrono::duration_cast<TDuration>(after - before);
            return duration.count();
        }

        bool elapsed_equals(double t2) {
            double t1 = elapsed();
            return fabs(t1 - t2) <= std::numeric_limits<double>::epsilon() * std::max({ 1.0, fabs(t1) , fabs(t2) });
        }

        static std::string clock_profile_string() {
            std::stringstream ss;
            ss << "clock profile: " << typeid(TClock).name() << " " << TClock::period::num << "/" << TClock::period::den << " second" << ((TClock::is_steady) ?"(steady)" :"(*not* steady)") << std::endl;
            return ss.str();
        }

    private:

        std::chrono::time_point<TClock> before;
        std::chrono::time_point<TClock> after;

    };

}

#endif // STOPWATCH_H
