#ifndef ACTIVE_LOG_INI_H
#define ACTIVE_LOG_INI_H

#include <string>

namespace liblog {

    constexpr size_t CAPACITY = 1024;
    constexpr char CSV = ',';

    const std::string INFO_MESSAGE = "Active Object Logger(β) v0.1 Fields: line_number, CLF_time, message";

}

#endif // ACTIVE_LOG_INI_H
