#ifndef ACTIVE_OSTREAM_H
#define ACTIVE_OSTREAM_H

#include <thread>
#include <ostream>
#include <sstream>
#include <mutex>

#include "mpmc_bounded_queue.h"

namespace aob {

    template<size_t capacity>
    class active_ostream {

        using queue_t = que::mpmc_bounded_queue<std::string, capacity>;
        using lock_t = std::unique_lock<std::mutex>;

    public:

        using message_t = std::string;

        active_ostream(std::ostream& out = std::cout):
            out_(out),
            t(std::unique_ptr<std::thread>(new std::thread([this] {
                this->run();
            }))) {}

        template<typename T>
        active_ostream& operator<<(const T& s) {
            lock_t lock(mutex);
            std::stringstream ss;
            ss << s;
            send(ss.str());
            return *this;
        }

        virtual void send(message_t m) {
            q.wait_push(m);
        }

        bool busy()  {
            return !q.empty();
        }

        virtual ~active_ostream() {
            done = true;
            t->join();
        }

    private:

        void run() {
            message_t m;
            while(!done) {
                if(q.try_pop(m)) out_ << m;
                std::this_thread::yield();
            }
            while(!q.empty()) {
                if(q.try_pop(m)) out_ << m;
                std::this_thread::yield();
            }
        }

        bool done = false;
        queue_t q;
        std::ostream& out_;
        std::unique_ptr<std::thread> t;
        std::mutex mutex;

        active_ostream(const active_ostream&) = delete;

        void operator=(const active_ostream&) = delete;

    };

}

#endif // ACTIVE_OSTREAM_H
